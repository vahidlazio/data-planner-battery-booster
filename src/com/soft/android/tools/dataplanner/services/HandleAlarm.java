package com.soft.android.tools.dataplanner.services;

import java.lang.reflect.Method;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.util.Log;

public class HandleAlarm extends BroadcastReceiver {

	public HandleAlarm() {
		// TODO Auto-generated constructor stub
	}

	public boolean getDataIsEnabled(Context context) {
		try {
			final ConnectivityManager conman = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class conmanClass = Class
					.forName(conman.getClass().getName());
			Method setMobileDataEnabledMethod = conmanClass.getDeclaredMethod(
					"setMobileDataEnabled", Boolean.TYPE);
			Method getMobileDataEnabledMethod = conmanClass
					.getDeclaredMethod("getMobileDataEnabled");
			getMobileDataEnabledMethod.setAccessible(true);
			setMobileDataEnabledMethod.setAccessible(true);
			return ((Boolean) getMobileDataEnabledMethod.invoke(conman));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}

	public void setDataEnabled(Context context, boolean _f) {
		try {
			final ConnectivityManager conman = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class conmanClass = Class
					.forName(conman.getClass().getName());
			Method setMobileDataEnabledMethod = conmanClass.getDeclaredMethod(
					"setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);
			setMobileDataEnabledMethod.invoke(conman, _f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public long getInterval(Context context, boolean _f) {
		try {
			if (_f)
				return context.getSharedPreferences("Alarm", 0).getLong("On",
						1) * 1000 * 60;
			else
				return context.getSharedPreferences("Alarm", 0).getLong("Off",
						5) * 60 * 1000;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	}

	public void DisableDataAndSetAlarmForOffInterval(Context context) {
		try {
			setDataEnabled(context, false);
			startAlarm(context, getInterval(context, false));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void stopAlarm(Context context) {
		try {
			AlarmManager am = (AlarmManager) context.getSystemService(context
					.getApplicationContext().ALARM_SERVICE);
			Intent I = new Intent(context, HandleAlarm.class);
			PendingIntent P = PendingIntent.getBroadcast(context, 0, I, 0);
			am.cancel(P);
			P.cancel();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void startAlarm(Context context, long _t) {
		try {
			stopAlarm(context);
			AlarmManager am = (AlarmManager) context.getSystemService(context
					.getApplicationContext().ALARM_SERVICE);
			Intent I = new Intent(context, HandleAlarm.class);
			PendingIntent P = PendingIntent.getBroadcast(context, 0, I, 0);
			am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + _t, P);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void EnableDataAndSetAlarmForOnInterval(Context context) {
		try {
			setDataEnabled(context, true);
			startAlarm(context, getInterval(context, true));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		try {
			Log.d("Alo", "1");
			if (getDataIsEnabled(context)) {
				Log.d("Alo", "2");
				DisableDataAndSetAlarmForOffInterval(context);
				Log.d("Alo", "3");
			} else {
				Log.d("Alo", "4");
				EnableDataAndSetAlarmForOnInterval(context);
				Log.d("Alo", "5");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
