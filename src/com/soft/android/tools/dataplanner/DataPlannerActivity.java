package com.soft.android.tools.dataplanner;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.soft.android.tools.dataplanner.adapter.SettingAdapter;
import com.soft.android.tools.dataplanner.services.HandleAlarm;
import com.soft.android.tools.dataplanner.utils.clsSettingItem;

import de.ankri.views.Switch;

@SuppressLint("NewApi")
public class DataPlannerActivity extends SherlockActivity implements
		LicenseCheckerCallback {
	public void doLicenseChecker() {
		try {
			if (mChecker == null) {
				mChecker = new LicenseChecker(this, new ServerManagedPolicy(
						this, new AESObfuscator(SALT, getPackageName(),
								mDeviceID)), BASE64_PUBLIC_KEY);
			}
			mChecker.checkAccess(this);
			Log.d("Check", "1");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	LicenseChecker mChecker;
	private static final byte[] SALT = new byte[] { -46, 65, 30, -128, -103,
			-57, 74, -64, 51, 88, -95, -45, 77, -117, -36, -113, -11, 32, -64,
			89 };
	private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkUj7HRlsh3pvngvJICkG8jV7BxV9cIRX2208Qqalvgp7d456zXKwzmMEXqH4wN96CrmWdEXyZadp9Gsak+BddvG4k/ymXF/g11r5AzS7S9ocOkHOPr3+zbQ9Kql3cv/LsWLYTOU9kG5LONGvz1lOWlYRViZGSzNJHnINHCRxqQdIQBLeDKuzKScEh5vD7faslOy2IklUgGF+4ZVA/wXCMk2KfOqT0wgf11JaYo/BFI9bIi6YPYpTi/4S2rUs4DvgFmcdvAD8QCykp0OQDlKnl/0HvdvfitBpNTUcpj2y5ssv33wbpjON2hVt8EOq099GN9HjK8ot6QsMHmdbR7K+oQIDAQAB";
	private String mDeviceID = "";

	SettingAdapter mAdapter;
	ArrayList<clsSettingItem> mList = new ArrayList<clsSettingItem>();

	public void setServiceIsStarted(boolean _f) {
		try {
			Editor mEdit = getSharedPreferences("Alarm", 0).edit();
			mEdit.putBoolean("Started", _f);
			mEdit.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void setServiceInterval(String _t, long _f) {
		try {
			Editor mEdit = getSharedPreferences("Alarm", 0).edit();
			mEdit.putLong(_t, _f);
			mEdit.commit();
			mAdapter.notifyDataSetChanged();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public boolean getServiceIsStarted() {
		try {
			return getSharedPreferences("Alarm", 0)
					.getBoolean("Started", false);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}

	OnCheckedChangeListener mListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			try {
				if (isChecked) {
					Intent mIntent = new Intent(getApplicationContext(),
							HandleAlarm.class);
					sendBroadcast(mIntent);
					Toast.makeText(getApplicationContext(),
							"Service has been started successfully",
							Toast.LENGTH_LONG).show();
				} else {
					stopAlarm(getApplicationContext());
					Toast.makeText(getApplicationContext(),
							"Service has been stopped successfully",
							Toast.LENGTH_LONG).show();
				}
				setServiceIsStarted(isChecked);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_data_planner);
		mDeviceID = Settings.Secure.getString(getApplicationContext()
				.getContentResolver(), Settings.Secure.ANDROID_ID);
		// if(true)
		// doAllThings();
		// return;
		if (getSharedPreferences("Alarm", 0).getBoolean("isVerified", false)) {
			doAllThings();
		} else {
			Toast.makeText(getApplicationContext(),
					"Please wait a sec for being verified", Toast.LENGTH_LONG)
					.show();
			doLicenseChecker();
		}
	}

	public void stopAlarm(Context context) {
		try {
			AlarmManager am = (AlarmManager) context.getSystemService(context
					.getApplicationContext().ALARM_SERVICE);
			Intent I = new Intent(context, HandleAlarm.class);
			PendingIntent P = PendingIntent.getBroadcast(context, 0, I, 0);
			am.cancel(P);
			P.cancel();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	AlertDialog alertDialog;

	public void showDialogForSaveOffInterval() {
		try {
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					this);
			final EditText userInput = new EditText(this);
			alertDialogBuilder.setTitle("Set OFF period interval Minutes");
			alertDialogBuilder.setView(userInput);
			alertDialogBuilder.setPositiveButton("Save",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							try {
								try {
									long m = Long.parseLong(userInput.getText()
											.toString());
									if (m >= 1) {
										setServiceInterval("Off", m);
									} else {
										Toast.makeText(getApplicationContext(),
												"Number is not valid",
												Toast.LENGTH_LONG).show();
										return;
									}

								} catch (Exception e) {
									// TODO: handle exception
									Toast.makeText(getApplicationContext(),
											"Number is not valid",
											Toast.LENGTH_LONG).show();
									return;
								}
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					});
			// set dialog message
			alertDialogBuilder.setCancelable(true);
			// create alert dialog
			alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showDialogForSaveOnInterval() {
		try {
			try {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						this);
				final EditText userInput = new EditText(this);
				alertDialogBuilder.setTitle("Set ON period interval Minutes");
				alertDialogBuilder.setView(userInput);
				alertDialogBuilder.setPositiveButton("Save",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								try {
									try {
										long m = Long.parseLong(userInput
												.getText().toString());
										if (m >= 1) {
											setServiceInterval("On", m);
										} else {
											Toast.makeText(
													getApplicationContext(),
													"Number is not valid",
													Toast.LENGTH_LONG).show();
											return;
										}

									} catch (Exception e) {
										// TODO: handle exception
										Toast.makeText(getApplicationContext(),
												"Number is not valid",
												Toast.LENGTH_LONG).show();
										return;
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
						});
				// set dialog message
				alertDialogBuilder.setCancelable(true);
				// create alert dialog
				alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			} catch (Exception e) {
				// TODO: handle exception
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void allow(int reason) {
		// TODO Auto-generated method stub
		try {
			mHandler.post(mRunnableSuccess);
			Editor mEdit = getSharedPreferences("Alarm", 0).edit();
			mEdit.putBoolean("isVerified", true);
			mEdit.commit();
			UiHandler.post(doAllThingsRun);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	Handler UiHandler = new Handler();
	Runnable doAllThingsRun = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				doAllThings();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	};

	public void doAllThings() {
		try {
			clsSettingItem mSetting = new clsSettingItem();
			mSetting.setTitle("How many minutes you prefer for OFF period interval");
			mSetting.setTypeIsOn(false);
			mList.add(mSetting);
			clsSettingItem mSetting1 = new clsSettingItem();
			mSetting1
					.setTitle("How many minutes you prefer for ON period interval");
			mSetting1.setTypeIsOn(false);
			mList.add(mSetting1);
			mAdapter = new SettingAdapter(getApplicationContext(), mList, 0);
			((ListView) findViewById(R.id.LstSettings)).setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();
			((ListView) findViewById(R.id.LstSettings))
					.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int arg2, long arg3) {
							// TODO Auto-generated method stub
							try {
								if (arg2 == 0)
									showDialogForSaveOffInterval();
								else
									showDialogForSaveOnInterval();
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					});
			// Intent mIntent = new
			// Intent(getApplicationContext(),HandleAlarm.class);
			// sendBroadcast(mIntent);
			((Switch) findViewById(R.id.switch_a))
					.setOnCheckedChangeListener(null);
			((Switch) findViewById(R.id.switch_a))
					.setChecked(getServiceIsStarted());
			((Switch) findViewById(R.id.switch_a))
					.setOnCheckedChangeListener(mListener);
			if (Build.VERSION.SDK_INT >= 12) {
				((RelativeLayout) findViewById(R.id.RelTitle)).setAlpha(0);
				((RelativeLayout) findViewById(R.id.RelTitle)).animate()
						.alpha(1).setDuration(2000).start();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void dontAllow(int reason) {

		// TODO Auto-generated method stub
		try {
			Toast.makeText(getApplicationContext(),
					"Please Download from Google Play", Toast.LENGTH_LONG)
					.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	Runnable mRunnableDont = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				Toast.makeText(getApplicationContext(),
						"Please Download from Google Play", Toast.LENGTH_LONG)
						.show();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	};
	Runnable mRunnableSuccess = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				Toast.makeText(getApplicationContext(), "You are verified now",
						Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	};

	Handler mHandler = new Handler();

	@Override
	public void applicationError(int errorCode) {
		// TODO Auto-generated method stub
		try {
			mHandler.post(mRunnableDont);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
