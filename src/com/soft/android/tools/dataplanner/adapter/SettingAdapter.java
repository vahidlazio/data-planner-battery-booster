package com.soft.android.tools.dataplanner.adapter;

import java.util.ArrayList;

import com.soft.android.tools.dataplanner.R;
import com.soft.android.tools.dataplanner.utils.clsSettingItem;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SettingAdapter extends ArrayAdapter {

	ArrayList<clsSettingItem> mList = new ArrayList<clsSettingItem>();

	public SettingAdapter(Context context, ArrayList<clsSettingItem> _List,
			int textViewResourceId) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
		mList = null;
		mList = _List;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		try {

			return mList.size();

		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	}
	public long getInterval(boolean _f)
	{
		try {
		
		if(!_f)
			return getContext().getSharedPreferences("Alarm", 0).getLong("Off", 5);
		else
			return getContext().getSharedPreferences("Alarm", 0).getLong("On", 1);
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}
	public String getSubTitle(int position) {
		try {
			if (position == 0) {
				return "Currently it's set to " + getInterval(false)
						+ " minutes, tap to change!";
			}
			else
				return "Currently it's set to " + getInterval(true)
						+ " minutes, tap to change!";
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "";
	}
	Interpolator interpolator = new DecelerateInterpolator();
	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		try {
			LayoutInflater mInflater = (LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View RowView = mInflater.inflate(
					
					R.layout.layout_item_setting, null);
			((TextView) RowView
					.findViewById(R.id.TxtTitle))
					.setText(mList.get(position).getTitle());
			((TextView) RowView
					.findViewById(R.id.TxtSubTitle))
					.setText(getSubTitle(position));
			if (Build.VERSION.SDK_INT >= 12) {
				RowView.setScaleX(0.01f);
				RowView.setScaleY(0.01f);
				ViewPropertyAnimator localViewPropertyAnimator = RowView
						.animate().scaleX(1).scaleY(1).setDuration(800)
						.setInterpolator(interpolator);

				localViewPropertyAnimator.setStartDelay(0).start();

			}
			return RowView;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

}
