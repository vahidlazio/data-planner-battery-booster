package com.soft.android.tools.dataplanner.utils;

public class clsSettingItem {

	private String Title = "";
	private String SubTitle = "";
	private boolean mTypeIsOnInterval = false; 
	public clsSettingItem() {
		// TODO Auto-generated constructor stub
	}
	public void setTitle(String _s)
	{
		Title = _s;
	}
	public String getTitle()
	{
		return Title;
	}
	public void setSubTitle(String _s)
	{
		try {
			SubTitle = _s;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public String getSubTitle()
	{
		return SubTitle;
	}
	public void setTypeIsOn(boolean _f)
	{
		mTypeIsOnInterval = _f;
	}
	public boolean getTypeIsOn()
	{
		return mTypeIsOnInterval;
	}

}
